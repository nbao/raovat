<?php

# Databases.
$databases['default']['default'] = [
  'collation' => 'utf8mb4_general_ci',
  'database' => 'raovat',
  'driver' => 'mysql',
  'host' => '127.0.0.1',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'password' => 'root',
  'port' => 3306,
  'prefix' => '',
  'username' => 'root',
];

// Prod: FALSE, Dev/Staging: TRUE
assert_options(ASSERT_ACTIVE, TRUE);
\Drupal\Component\Assertion\Handle::register();

// Prod: commented. Dev/Staging: uncommented.
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';

$local_services = "${app_root}/../settings/services.local.yml";
if (is_readable($local_services)) {
  $settings['container_yamls'][] = $local_services;
}
unset($local_services);

$config['system.logging']['error_level'] = 'verbose';

$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

/*
 * Do not use this setting until after the site is installed.
 */
// $settings['cache']['bins']['render'] = 'cache.backend.null';

// $settings['cache']['bins']['discovery_migration'] = 'cache.backend.memory';

// $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

$settings['extension_discovery_scan_tests'] = FALSE;

$settings['rebuild_access'] = TRUE;

$settings['skip_permissions_hardening'] = TRUE;

$settings['trusted_host_patterns'] = [
  '^www\.raovat\.fr$',
  '^.+\.raovat\.fr',
  '^raovat\.localhost$',
];

//$config_directories['sync'] = realpath("${app_root}/../config");
//$config['locale.settings']['translation']['path'] = realpath("${app_root}/../import/translations");
//$config['locale.settings']['translation']['use_source'] = 'local';
$settings['update_free_access'] = FALSE;

$settings['rabbitmq_credentials'] = [
  'host' => 'localhost',
  'port' => 5672,
  'username' => 'user',
  'password' => 'bitnami',
  'vhost' => '/'
];
//$settings['cache']['default'] = 'cache.backend.redis';
//$settings['queue_default'] = 'queue.rabbitmq';
