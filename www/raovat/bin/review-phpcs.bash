#!/usr/bin/env bash

# This scripts accepts a PHPCSOPTS environment variable to add options.
# It outputs its XML results in ../logs/phpcs.xml

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/review-common.bash"

phpcs \
  --config-set installed_paths "${PWD}/vendor/drupal/coder/coder_sniffer" \
  > /dev/null

phpcs ${PHPCSOPTS}  \
  --colors          \
  --standard=PSR1,Drupal,DrupalPractice \
  --extensions=php,module,inc,install,test,profile,theme,css,info,txt \
  --report=checkstyle --report-file="${DIR}/../logs/phpcs.xml" \
  --ignore="magpjt/bootstrap,magpjt/fonts" \
  ${DIRS}

# We don't want the build to fail because just because of review errors.
exit 0
