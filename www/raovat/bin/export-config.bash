#!/usr/bin/env bash

# Export active configuration from a Drupal 8 site.
#
# The script assumes a working DB configuration available in:
#   settings/settings.local.php
# customized from:
#   settings/example.settings.local.php
#
# Authors:
#  - NGUYEN Bao
#
# Usage:
#
#  composer export-conf

. bin/site-lib.bash

# Maintenance ON (site not accessible).
maintenance 1

# Disable all developement modules to exclude their configuration.
install_modules pmu

# Perform export.
$DRUSH cex -y

# Re-enable development modules.
install_modules en

# Maintenance OFF
maintenance 0
