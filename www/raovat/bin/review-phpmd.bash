#!/usr/bin/env bash

# This scripts accepts a PHPMDOPTS environment variable to add options.
# It outputs its XML results in ../logs/phpmd.xml

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/review-common.bash"

# Unlike most commands, PHPMD options go AFTER the 3 required arguments.
phpmd           \
  $COMMADIRS    \
  xml           \
  cleancode,codesize,controversial,design,naming,unusedcode \
  ${PHPMDOPTS}  \
  --reportfile "${DIR}/../logs/phpmd.xml"

# We don't want the build to fail because just because of review errors.
exit 0
