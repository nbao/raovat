#!/usr/bin/env bash

# Library of globals and functions to support site deployment scripts.
#
# The script assumes a working DB configuration available in:
#   settings/settings.local.php
# customized from:
#   settings/example.settings.local.php
#
# Files needed by this script in settings/ directory:
# - drush-config.bash
# - settings.php
#
# Use of local customizations like settings.local.php and services.local.yml
# is performed by committed files and needs no manual copying.
#
# Authors:
#  - NGUYEN Bao
#
# Usage: source in site deployment script.

PWD=$(pwd)

DRUSH="${PWD}/vendor/bin/drush -r ${PWD}/web"
SETTINGS="${PWD}/settings"

# Include settings script.
. "${PWD}/bin/drush-config.bash"
# Copy $file from $src to $dst.
function copy {
  local src=$1  # The source directory.
  local dst=$2  # The destination directory
  local file=$3 # The name of the file to copy

  # Only work if there is something to copy.
  if [ -f "${src}/${file}" ]; then
    # Ensure deletion of original, and only if needed.
    if [ -f "${dst}/${file}" ]; then
      chmod +w "${dst}"
      chmod +w "${dst}/${file}"
      rm -f "${dst}/${file}"
    fi
    cp "${src}/${file}" "${dst}/${file}"
  else
    echo "Nothing to copy for ${file}. Hope it's OK."
  fi
}

function install_modules() {
  local cmd=${1:-en}

  if [ ! -z "$DRUSH_INSTALL_MODULES" ]; then
    # Note: do not quote-wrap this variable to allow its space-separated content
    # to be passed as multiple arguments. None of them may contain a space
    # anyway, since they are Drupal module names.
    $DRUSH "${cmd}" -y $DRUSH_INSTALL_MODULES
  fi
}

function maintenance() {
  local state=${1:-1}
  $DRUSH state-set system.maintenance_mode $state
}

# Set the uuid property on a configuration variable.
function set_uuid {
  local entity=$1
  local uuid=$2
  $DRUSH cset -y "${entity}" uuid "${uuid}"
}

# Enable Redis
function enable_redis() {
  LINE='$settings'"['cache']['default'] = 'cache.backend.redis';"
  FILE="$SETTINGS/settings.local.php"
  grep -q -F "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
}

# Disable Redis
function disable_redis() {
  PATTERN="/cache.backend.redis/d"
  FILE="$SETTINGS/settings.local.php"
  TMP_FILE="$SETTINGS/settings.temp.php"
  sed "${PATTERN}" "$FILE" > "$TMP_FILE" && mv "$TMP_FILE" "$FILE"
}

function unset_queue_default() {
  PATTERN="/queue_default/d"
  FILE="$SETTINGS/settings.local.php"
  TMP_FILE="$SETTINGS/settings.temp.php"
  sed "${PATTERN}" "$FILE" > "$TMP_FILE" && mv "$TMP_FILE" "$FILE"
}

function set_queue_default() {
  local queueservice=$1

  LINE='$settings'"['queue_default'] = '${queueservice}';"
  FILE="$SETTINGS/settings.local.php"
  grep -q -F "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
}
