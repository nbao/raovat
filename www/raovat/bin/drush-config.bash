#!/usr/bin/env bash

# Admin informations.
export DRUSH_INSTALL_ACCOUNT_NAME="admin"
export DRUSH_INSTALL_ACCOUNT_PASS="admin"
export DRUSH_INSTALL_ACCOUNT_MAIL="n.bao@yahoo.com"
export DRUSH_INSTALL_LOCALE="vi"
export DRUSH_INSTALL_NAME="Rao vat"

# You must have this modules into composer.json to correctly install this modules.
export DRUSH_INSTALL_MODULES="admin_toolbar devel kint features_ui"
