#!/usr/bin/env bash

DRUSH="${PWD}/vendor/bin/drush -r ${PWD}/web"
PREFIX=reinstall

# Do NOT quote $STAGERS when reading it to preserve spaces. Order matters.
STAGERS="snp_staging reinstall migrate_tools migrate_plus migrate"

# Activate staging module
$DRUSH en $STAGERS -y

# Users
$DRUSH mi --update snp_staging_user

# Reset password on dev
$DRUSH upwd "Jean-Philippe Talamon" --password=Jesuisunpo12!
$DRUSH upwd "Felix Fatalot" --password=Jesuisunpo12!
$DRUSH upwd "Adrien Fonteneau" --password=Jesuisunpo12!

# Files
$DRUSH mi reinstall_files
cp -r import/file/public/* web/sites/default/files

# Taxonomy terms
$DRUSH mi snp_staging_taxonomy_channel
$DRUSH mi reinstall_terms_theme

# Programm

# Menu
$DRUSH mi --update reinstall_menusnp

# Articles

# Pages

# Sponsoring
$DRUSH mi --update reinstall_sponsoring

exit

exit

# Uninstall import modules.
for module in $STAGERS; do
  $DRUSH pmu $module -y
done
