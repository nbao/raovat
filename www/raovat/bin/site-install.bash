#!/usr/bin/env bash

# Install the site after deployment.
#
# The script assumes a working DB configuration available in:
#   settings/settings.local.php
# customized from:
#   settings/example.settings.local.php
#
# Files needed by this script in settings/ directory:
# - drush-config.bash
# - settings.php
# - services.yml
# - development.services.yml
# - phpunit.xml.dist -> customize from example.phpunit.xml.dist
#
# Use of local customizations like settings.local.php and services.local.yml
# is performed by committed files and needs no manual copying.
#
# Usage:
#
#  composer site-install

. bin/site-lib.bash

# Switch back to drupal queues if needed.
unset_queue_default

disable_redis

SITES="${PWD}/web/sites"
DEFAULT="${SITES}/default"
FILES="${DEFAULT}/files"

# Prepare local configuration files: remove core default and add ours.
copy "${SETTINGS}" "${DEFAULT}" settings.php
copy "${SETTINGS}" "${DEFAULT}" services.yml
copy "${SETTINGS}" "${SITES}" development.services.yml
copy "${SETTINGS}" "${PWD}" phpunit.xml.dist

# Prepare the files directory by cleaning it and re-creating it.
if [ -d "${FILES}" ]; then
  chmod -Rf +wx "${FILES}"
  rm -rf "${FILES}"
fi
if [ ! -d "${FILES}" ]; then
  mkdir -m777 "${FILES}"
fi
echo $DRUSH site-install -y  --account-mail="${DRUSH_INSTALL_ACCOUNT_MAIL}" --account-name="${DRUSH_INSTALL_ACCOUNT_NAME}" --account-pass="${DRUSH_INSTALL_ACCOUNT_PASS}" --locale="${DRUSH_INSTALL_LOCALE}"  --site-mail="${DRUSH_INSTALL_ACCOUNT_MAIL}" --site-name="${DRUSH_INSTALL_NAME}"
# Install Drupal.
$DRUSH site-install -y \
  --account-mail="${DRUSH_INSTALL_ACCOUNT_MAIL}" \
  --account-name="${DRUSH_INSTALL_ACCOUNT_NAME}" \
  --account-pass="${DRUSH_INSTALL_ACCOUNT_PASS}" \
  --locale="${DRUSH_INSTALL_LOCALE}" \
  --site-mail="${DRUSH_INSTALL_ACCOUNT_MAIL}" \
  --site-name="${DRUSH_INSTALL_NAME}"
$DRUSH pmu -y update

# Overwrite settings modifications performed by site installation.
copy "${SETTINGS}" "${DEFAULT}" settings.php

# Cache rebuild.
$DRUSH cr

# Enforce system.site uuid to avoid deployment issues.
set_uuid system.site "d85720c4-4443-4d7e-a2f2-8a5a4dad36bd"

# Enforce language.entity.fr uuid to prevent the attempt to remove the default
# language configuration.
set_uuid language.entity.vi "20e03280-7f11-475f-9db4-0b67971ba3e9"
set_uuid language.entity.und "3a57636a-0f4b-4ee7-8e9a-8dcc4f6f4de2"
set_uuid language.entity.zxx "922c61be-3eb3-4ffb-8f0e-9976ff3d0c8e"

# Enable additional modules listed in `/../settings/drush-config.bash`.
install_modules en

enable_redis

# Switch to rabbitmq queues.
set_queue_default "queue.rabbitmq"

# Language imports.
$DRUSH language-import fr ${PWD}/import/translations/ext/*.vi.po
