#!/usr/bin/env bash

# This scripts accepts a PHPCPDOPTS environment variable to add options.
# It outputs its XML results in ../logs/phpcpd.xml

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/review-common.bash"

phpcpd ${PHPCPDOPTS}                    \
  --fuzzy                               \
  --log-pmd="${DIR}/../logs/phpcpd.xml" \
  --min-lines=3                         \
  --min-tokens=30                       \
  --names="*.php,*.module,*.inc,*.install,*.test,*.profile,*.theme" \
  ${DIRS}

# We don't want the build to fail because just because of review errors.
exit 0
