# Native docker-based local environment for Drupal

Use this Docker compose file to spin up local environment for [Drupal](https://wwww.drupal.org) with a *native Docker app*

This docker setup works with **Ubuntu 16**, **Nginx**, **MariaDB 10.2/**, **PHP 7.1** and **Elasticsearch**.

Install Drupal 8
- change to www/raovat and run 

```bash
composer install 
```
Check if everything works fine

* Management Docker image <a href="http://portainer.localhost:8000/">http://portainer.localhost:8000/</a>
* PhpMyAdmin <a href="http://phpmyadmin.localhost:8000/">http://phpmyadmin.localhost:8000/</a>
* Queque RabbitMQ <a href="http://rabbitmq.localhost:8000/">http://rabbitmq.localhost:8000/</a>
* Monitoring Log Kibana <a href="http://kibana.localhost:8000/">http://kibana.localhost:8000/</a>
* Motor searching Elasticsearch <a href="http://es.localhost:8000/">http://es.localhost:8000/</a>
* Management Email <a href="http://mailhog.localhost:8000/">http://mailhog.localhost:8000/</a>
* Setup Drupal 8 <a href="http://raovat.localhost:8000/">http://raovat.localhost:8000/</a>
* Mail Mailhog  <a href="http://mailhog.localhost:8000/">http://mailhog.localhost:8000</a>

---

* [Overview](#overview)
* [Instructions](#instructions)
    * [Usage](#usage)
* [Containers](#containers)
* [Custom settings](#custom-settings)
    * [Varnish](#varnish-1)
        * [drupal-base.vcl for Drupal](#drupal-basevcl-for-drupal)
        * [Environment](#environment)
    * [Nginx PHP](#ngix-php-1)
        * [Web Data Volume](#web-data-volume)
        * [Apache Virtualhost](#apache-virtualhost)
        * [PHP](#php)
        * [SSH](#ssh)
        * [Environment](#environment-1)
    * [MySQL](#mysql-1)
        * [MySQL Data Volume](#mysql-data-volume)
        * [Custom my.cnf](#custom-mycnf)
        * [Environment](#environment-2)
    * [phpMyAdmin](#phpmyadmin-1)
        * [Environment](#environment-3)
    * [Mailhog](#mailhog-1)
    * [Elasticsearch](#elasticsearch-1)

## Overview

The [Drupal](https://wwww.drupal.org) bundle consist of the following containers:

| Container | Version | Service name | Image | Public Port | Enabled by default |
| --------- | ------- | ------------ | ----- | ----------- | ------------------ |
| [Varnish](#varnish) | [4.2](https://hub.docker.com/r/wodby/drupal-varnish/tags/) | varnish | [wodby/drupal-varnish](https://hub.docker.com/r/wodby/drupal-varnish/tags/) | 6081:80 | ✓ |
| [NGINX PHP](#ngix-php) | [7.1](https://github.com/wodby/drupal-nginx) | php | [wodby/drupal-nginx](https://hub.docker.com/r/wodby/drupal-nginx/tags/) | 8000:80 | ✓ |
| [MariaDB](#mariadb) | [10.1](https://downloads.mariadb.org/mariadb/10.1.26/) | mariadb | [wodby/mariadb](https://hub.docker.com/r/wodby/mariadb/tags/) | 3306 | ✓ |
| [phpMyAdmin](#phpmyadmin) | | phpmyadmin | <a href="https://hub.docker.com/r/phpmyadmin/phpmyadmin" target="_blank">phpmyadmin/phpmyadmin</a> |  8880 | ✓ |
| [Mailhog](#mailhog) | [1](https://github.com/mailhog/MailHog)| mailhog | <a href="https://hub.docker.com/r/mailhog/mailhog" target="_blank">mailhog/mailhog</a> | 8025 - 1025 | ✓ |
| [Elasticsearch](#elastic) | [5.5](https://www.elastic.co/fr/products/elasticsearch) | elasticsearch | elasticsearch | 9200 | ✓ |
| [Logstash](#logstash) | [5.5.1](https://www.elastic.co/fr/products/logstash) | logstash | logstash | 5000 | ✓ |
| [Kibana](#kibana) | [5.5.1](https://www.elastic.co/fr/products/kibana) | kibana | kibana | 5601 | ✓ |
| [RabittMQ](#rabittmq) | [3.6.10](https://www.rabbitmq.com/) | rabbitmq | rabittmq | 15672 | ✓ |
| [Portainer](#portainer) | [1](https://portainer.io/) | portainer | portainer/portainer | 8983 | ✓ |
| [Traefik](#trafik) | [1](https://docs.traefik.io/) | traefik | traefik | 8983 | ✓ |
| [Redis](#redis) | [4.5](https://redis.io/) | redis | redis | 8983 | ✓ |



## Instructions

**Feel free to adjust volumes and ports in the compose file for your convenience.**

### Usage

Run:

```bash
$ docker-compose up -d
```

Stop:

```bash
$ docker-compose stop
```
Or down (warning: this command remove volume changes):

```bash
$ docker-compose down
```

#### Run bash

```bash
docker exec -it php /bin/bash
```

Replace _dockerlamp_raovat_php_1_ with _name_ of: 

```bash
docker-compose ps
```

#### Import Database

After run containers

Connect to [PhpMyadmin](http://phpmyadmin.localhost:8000/) and import file [raovat.sql](data/raovat.sql) ind DB raovat

## Containers

### Varnish

Available tags are:

- 4.2, latest ([4.2/Dockerfile](https://github.com/keopx/docker-varnish/blob/master/4.0/Dockerfile))

### Nginx PHP
- 7.1, latest ([7.1/Dockerfile](https://github.com/keopx/docker-apache-php/blob/master/7.1/Dockerfile))

### MariaDb
- 10.2, latest ([10.2/Dockerfile](https://github.com/keopx/docker-mysql/blob/master/5.7/Dockerfile))

### phpMyAdmin

This is a default image. Only use to have a easy access to database information.

### MailHog

This is a default image. Use to have easy mailsender and mail watcher to test email without send to real account.

## Custom settings

### Varnish

By default we can use a standard _default.vcl_.

In addition, you can check a varnish vcl for [Drupal](https://www.drupal.org) in [drupal-base.vcl](https://github.com/keopx/docker-lamp/blob/master/config/varnish/drupal-base.vcl)

#### drupal-base.vcl for Drupal

You can check a special varnish vcl file for [Drupal](https://wwww.drupal.org) **drupal-base.vcl** based in [NITEMAN](https://github.com/NITEMAN) config file: [drupal-base.vcl](https://github.com/NITEMAN/varnish-bites/blob/master/varnish4/drupal-base.vcl)

**Note**: drupal-base.vcl uses MIT license.

If you like to add **drupal-base.vcl** add this lines. Added by default 
     
```yml
    volumes:
      - ./config/varnish/drupal-base.vcl:/etc/varnish/default.vcl
```

#### Environment

The first two lines works to setup a default varnish port and memory usage limit.

The second two lines only works to change **default.vcl** setup to run correctly.

_web_ is name of linked _apache-php_ image name.

```yml
    environment:
      - VARNISH_PORT=80
      - VARNISH_MEMORY=500M
      # Next values only works with default default.vcl file.
      - VARNISH_BACKEND_IP=web
      - VARNISH_BACKEND_PORT=80
```

### Apache PHP

#### Web Data Volume

```yml
    volumes:
      - ./www/raovat:/var/www/raovat # Data.
```


For reload system:

```bash
$ docker-compose stop
$ docker-compose up -d
```

#### PHP

Use some setup by default. You can (un)comment to change behaviour.

You can see **two _php.ini_ templates** with different setup, [development](https://github.com/keopx/docker-lamp/blob/master/config/php/php.ini-development) and [production](https://github.com/keopx/docker-lamp/blob/master/config/php/php.ini-production) setup.

In addition, you can check **xdebug** and **xhprof** configuration, the same file for php 7.1, 7.0 and 5.6, and  **opcache** recomended file version for [Drupal](https://wwww.drupal.org).

##### PHP 7.1

```yml
      # php.ini for php 7.7 and remove environment varibles.
      - ./config/php/php.ini:/etc/php7/apache2/php.ini
```

e.g.: if you need add more PHP memory_limit modify _./config/php-{version}/php.ini_ file and reload system to works:

```bash
$ docker-compose stop
$ docker-compose up -d
```
run this command to get ip of container docker
```bash
docker network list
docker network inspect raovat_default
```
replace ip in PHP_XDEBUG_REMOTE_HOST : "ip_docker"
something like this
```bash
PHP_XDEBUG_REMOTE_HOST: "172.18.0.1"  # You will also need to 'sudo ifconfig lo0 alias 10.254.254.254'
```
to Xdebug in PHPStorm
172.18.0.1

![phpstorm_xdebug](/uploads/7d4f2a09a1c6c03225712abc2831e0ef/phpstorm_xdebug.png)
#### Environment

You can check in docker-composer.yml two special environment variable to setup SMTP service to test local emails.

The _apache-php_ has _ssmtp_ sender package. Here default setup to run by default with mailhog.

Use to connect to MailHog **mail** instead *localhost*.

```yml
    environment:
      # ssmtp mail sender.
      - PHP_SENDMAIL_PATH="/usr/sbin/ssmtp -t"
      # SMTP server configruation: "domain:port" | "mail" server domain is mailhog name.
      - PHP_SENDMAIL_DOMAIN=mail:1025
```

### MySQL

Use to connect to MySQl **mysql** instead *localhost*.

#### MySQL Data Volume

```yml
    volumes:
      - ./data/database:/var/lib/mysql
```
#### Custom my.cnf

You can check [my.cnf](https://github.com/keopx/docker-lamp/blob/master/config/mysql/my.cnf) and change you need variables.

```yml
      ## Custom setup for MySQL
      - ./config/mysql/my.cnf:/etc/mysql/my.cnf
```

#### Environment

* MYSQL_ROOT_PASSWORD: The password for the root user. Defaults to a blank password.
* MYSQL_DATABASE: A database to automatically create. If not provided, does not create a database.
* MYSQL_USER: A user to create that has access to the database specified by MYSQL_DATABASE.
* MYSQL_PASSWORD: The password for MYSQL_USER. Defaults to a blank password.

```yml
    environment:
      - MYSQL_ROOT_PASSWORD=root
      - MYSQL_DATABASE=raovat
      - MYSQL_USER=root
      - MYSQL_PASSWORD=root
```

### phpMyAdmin

Use to connect to MySQl **mysql** instead *localhost*.

#### Environment

* PMA_HOST: Host to connect phpMyAdmin.

```yml
    environment:
      - PMA_HOST=mysql
```

### MailHog

Default image and setup.
