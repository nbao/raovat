<?php

# Databases.
$databases['default']['default'] = [
    'collation' => 'utf8mb4_general_ci',
    'database' => 'raovat',
    'driver' => 'mysql',
    'host' => '127.0.0.1',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'password' => 'root',
    'port' => 3306,
    'prefix' => '',
    'username' => 'root',
];

$config['system.logging']['error_level'] = 'verbose';

$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

$settings['extension_discovery_scan_tests'] = FALSE;

$settings['rebuild_access'] = TRUE;

$settings['skip_permissions_hardening'] = TRUE;

$settings['update_free_access'] = FALSE;

$settings['rabbitmq_credentials'] = [
    'host' => 'localhost',
    'port' => 5672,
    'username' => 'user',
    'password' => 'bitnami',
    'vhost' => '/'
];
//$settings['cache']['default'] = 'cache.backend.redis';
//$settings['queue_default'] = 'queue.rabbitmq';
